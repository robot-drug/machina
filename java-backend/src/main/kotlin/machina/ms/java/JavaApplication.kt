package machina.ms.java

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class JavaApplication

fun main(args: Array<String>) {
	runApplication<JavaApplication>(*args)
}
